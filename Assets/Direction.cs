﻿using UnityEngine;
using System.Collections;

public class Direction
{
    private static Vector2 _up = new Vector2(0, 1);
    private static Vector2 _down = new Vector2(0, -1);
    private static Vector2 _left = new Vector2(-1, 0);
    private static Vector2 _right = new Vector2(1, 0);
    private static Vector2 _upRight = new Vector2(1, 1);
    private static Vector2 _upLeft =  new Vector2(-1, 1);
    private static Vector2 _downRight = new Vector2(1, -1);
    private static Vector2 _downLeft = new Vector2(-1, -1);
    private static Vector2 _stay = new Vector2(0, 0);

    public static Vector2 Up
    {
        get
        {
            return _up;
        }
               
    }

    public static Vector2 Down
    {
        get
        {
            return _down;
        }
    }

    public static Vector2 Left
    {
        get
        {
            return _left;
        }
    }

    public static Vector2 Right
    {
        get
        {
            return _right;
        }
    }

    public static Vector2 UpRight
    {
        get
        {
            return _upRight;
        }
    }

    public static Vector2 UpLeft
    {
        get
        {
            return _upLeft;
        }
    }

    public static Vector2 DownRight
    {
        get
        {
            return _downRight;
        }
    }

    public static Vector2 DownLeft
    {
        get
        {
            return _downLeft;
        }
    }

    public static Vector2 Stay
    {
        get
        {
            return _stay;
        }
    }
}
