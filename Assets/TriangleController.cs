﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleController : BoardController
{

    public TileController tile;
    //public GameObject player;
    public bool ChooseTile;
    void Start()
    {
        tile = GetComponentInParent<TileController>();
       // ChooseTileToMove().GetComponent<SpriteRenderer>().color = Color.red;
    }

    // Update is called once per frame
    void Update()
    {

        

    }

    GameObject ChooseTileToMove()
    {
        System.Array.Sort(tile.Neighbours, delegate (GameObject col1, GameObject col2)
             {
                 return Vector3.Distance(col1.transform.position, BoardController.Player.transform.position).CompareTo(Vector3.Distance(col2.transform.position, BoardController.Player.transform.position));
             }
          );

        for (int i = 0; i < tile.Neighbours.Length; i++)
        {
            if (CheckIfInSight(tile.Neighbours[i].gameObject))
            {
                return tile.Neighbours[i];
            }
        }

        return tile.Neighbours[0];

    }

    bool CheckIfInSight(GameObject tileToCheck)
    {
        Vector2 targetDelta = new Vector2(tileToCheck.transform.position.x - Player.transform.position.x, tileToCheck.transform.position.y - Player.transform.position.y);
        Vector2 directionToShoot;

        if (targetDelta.x == 0 || targetDelta.y == 0)
        {
            return true;
        }

        if(targetDelta.x > 0)
        {
            if(targetDelta.y > 0)
            {
                directionToShoot = Direction.UpRight;
            }
            else
            {
                directionToShoot = Direction.DownRight;
            }
        }
        else
        {
            if (targetDelta.y > 0)
            {
                directionToShoot = Direction.UpLeft;
            }
            else
            {
                directionToShoot = Direction.DownLeft;
            }
        }

        if (Physics2D.Raycast(/*Vector2 origin*/ tileToCheck.transform.position, /*Vector2 direction*/ directionToShoot, /*float distance*/ Mathf.Infinity, /*int layerMask*/LayerMask.GetMask("Blob"), /*float minDepth*/Mathf.NegativeInfinity, /*float maxDepth*/Mathf.Infinity))
        {
            return true;
        }

        return false;

        
    }


    public void SwitchTile()
    {
        transform.SetParent(ChooseTileToMove().transform);
        transform.localPosition = new Vector3(0, 0, 0);
    }



}
