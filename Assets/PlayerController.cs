﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BoardController {

    public bool combo = false;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject CheckIfHitEnemy(Vector2 Direction)
    {
        return Physics2D.Raycast(/*Vector2 origin*/ Player.transform.position, /*Vector2 direction*/ Direction, /*float distance*/ Mathf.Infinity, /*int layerMask*/LayerMask.GetMask("Enemy"), /*float minDepth*/Mathf.NegativeInfinity, /*float maxDepth*/Mathf.Infinity).transform.gameObject;
    }

    public void SwitchTile(Vector2 direction)
    {
        GameObject tempEnemy = CheckIfHitEnemy(direction);

        if(tempEnemy != null)
        {
            transform.SetParent(tempEnemy.transform.parent.transform);
            Enemies.Remove(tempEnemy);
            Destroy(tempEnemy);
            transform.localPosition = new Vector3(0, 0, 0);
            combo = true;
        }
        else
        {
            combo = false;
        }
    }
}
