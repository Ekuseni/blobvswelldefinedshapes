﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BoardController : MonoBehaviour
{
    public static bool enemyTurn = false;

    public static int _width = 16;
    public static int _height = 9;

    private static GameObject[,] _board;
    private static List<GameObject> _enemies = new List<GameObject>();

    private static GameObject _player = null;
    private static GameObject _tile = null;
    private static GameObject _triangle = null;


    public static GameObject Player
    {
        get
        {
            return _player;
        }
    }

    private static int Width
    {
        get
        {
            return _width;
        }
        set
        {
            _width = value;
        }
    }

    private static int Height
    {
        get
        {
            return _height;
        }
        set
        {
            _height = value;
        }
    }

    private static GameObject[,] Board
    {
        get
        {
            return _board;
        }
    }

    private static GameObject Triangle
    {
        get
        {
            return _triangle;
        }
    }

    public static List<GameObject> Enemies
    {
        get
        {
            return _enemies;
        }
    }

    // Use this for initialization
    void Start()
    {
        _board = new GameObject[_width, _height];
        _player = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/blob"));
        _tile = Resources.Load<GameObject>("Prefabs/tile");
        _triangle = Resources.Load<GameObject>("Prefabs/triangle");


        for (int i = 0; i < _width; i++)
        {
            for (int j = 0; j < _height; j++)
            {
                _board[i, j] = Instantiate<GameObject>(_tile, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), this.gameObject.transform);
            }
        }

        Player.transform.SetParent(GetRandomTile().transform);
        Player.transform.localPosition = new Vector3(0, 0, 0);



    }

    // Update is called once per frame
    void Update()
    {
        if (enemyTurn)
        {
            MoveEnemies();
            SpawnEnemies();
            enemyTurn = false;
        }
    }

    public static GameObject GetRandomTile()
    {
        int randomNum;
        int x;
        int y;
        

        randomNum = Random.Range(0, Width * Height);

        x = randomNum % Width;
        y = randomNum / Width;

        
        return Board[x,y];

    }

    private static void SpawnEnemy()
    {
        GameObject tempTile;
        GameObject enemyToSpawn;
        do
        {
            tempTile = GetRandomTile();
        }
        while (!tempTile.GetComponent<TileController>().CanSpawOn());

        enemyToSpawn = Instantiate<GameObject>(Triangle);

        enemyToSpawn.transform.SetParent(tempTile.transform);
        enemyToSpawn.transform.localPosition = new Vector3(0, 0, 0);
        Enemies.Add(enemyToSpawn);
    }

    private void MoveEnemies()
    {
        foreach (GameObject enemy in Enemies)
        {
            enemy.GetComponent<TriangleController>().SwitchTile();
        }
    }

    private void SpawnEnemies()
    {
        int maxEnemies = Mathf.CeilToInt(Width * Height / 2f);
        int enmiesToSpawnNum = 4;

        if(Enemies.Count < maxEnemies)
        {
            for(int i = 0;i < enmiesToSpawnNum; i++)
            {
                SpawnEnemy();
            }
        }


    }

    [CustomEditor(typeof(BoardController))]
    public class BoardControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
           BoardController boardController = (BoardController)target;


            /*First line of control buttons*/
            GUILayout.BeginHorizontal();
            if(GUILayout.Button("UpLeft"))
            {
                Player.GetComponent<PlayerController>().SwitchTile(Direction.UpLeft);
                enemyTurn = !Player.GetComponent<PlayerController>().combo;
            }
            if (GUILayout.Button("Up"))
            {
                Player.GetComponent<PlayerController>().SwitchTile(Direction.Up);
                enemyTurn = !Player.GetComponent<PlayerController>().combo;
            }
            if (GUILayout.Button("UpRight"))
            {
                Player.GetComponent<PlayerController>().SwitchTile(Direction.UpRight);
                enemyTurn = !Player.GetComponent<PlayerController>().combo;
            }
            GUILayout.EndHorizontal();
            /*End of first line of control buttons*/

            /*Second line of control buttons*/
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Left"))
            {
                Player.GetComponent<PlayerController>().SwitchTile(Direction.Left);
                enemyTurn = !Player.GetComponent<PlayerController>().combo;
            }
            if (GUILayout.Button(""))
            {
                enemyTurn = true;
            }
            if (GUILayout.Button("Right"))
            {
                Player.GetComponent<PlayerController>().SwitchTile(Direction.Right);
                enemyTurn = !Player.GetComponent<PlayerController>().combo;
            }
            GUILayout.EndHorizontal();
            /*End of second line of control buttons*/

            /*Third line of control buttons*/
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("DownLeft"))
            {
                Player.GetComponent<PlayerController>().SwitchTile(Direction.DownLeft);
                enemyTurn = !Player.GetComponent<PlayerController>().combo;
            }
            if (GUILayout.Button("Down"))
            {
                Player.GetComponent<PlayerController>().SwitchTile(Direction.Down);
                enemyTurn = !Player.GetComponent<PlayerController>().combo;
            }
            if (GUILayout.Button("DownRight"))
            {
                Player.GetComponent<PlayerController>().SwitchTile(Direction.DownRight);
                enemyTurn = !Player.GetComponent<PlayerController>().combo;
            }
            GUILayout.EndHorizontal();
            /*End of third line of control buttons*/
        }

    }

}
