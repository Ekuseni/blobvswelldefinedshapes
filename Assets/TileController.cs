﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileController : BoardController
{

    public bool ShowNeighbours = false;

    public GameObject[] _neighbours;

    public GameObject[] Neighbours
    {
        get
        {
            return _neighbours;
        }
    }

	void Start ()
    {

        _neighbours = GetNeighbours();

	}
	
	// Update is called once per frame
	void Update ()
    {
	
        //if(ShowNeighbours)
        //{
        //    foreach(Collider2D box in boxCast)
        //    {
        //        box.transform.GetComponent<SpriteRenderer>().color = Color.red;
        //    }
        //}
        
    }

    public GameObject[] GetNeighbours()
    {
        Collider2D[] boxCast;
        List<GameObject> listToReturn = new List<GameObject>();

        boxCast = Physics2D.OverlapBoxAll(new Vector2(this.transform.position.x, this.transform.position.y), new Vector2 (1.1f, 1.1f), 0, LayerMask.GetMask("Tile"), -Mathf.Infinity, Mathf.Infinity);

        foreach(Collider2D hit in boxCast)
        {
            listToReturn.Add(hit.gameObject);
        }

        return listToReturn.ToArray();
    }

    public bool CanSpawOn()
    {
        if (transform.childCount > 0)
        {
            return false;
        }

        if (Vector3.Distance(transform.position, Player.transform.position) < 3)
        {
            return false;
        }

        return true;
    }
}
